@extends('layouts.guest')
@section('content')
    <div class="card w-50">
        <div class="card-body">
            <form id="login-form" class="form" action="" method="post">
                @csrf
                <h3 class="text-center text-info">Login</h3>
                <div class="form-group">
                    <label for="username" class="text-info">Username:</label><br>
                    <input type="text" name="username" id="username" class="form-control">
                </div>
                <div class="form-group">
                    <label for="password" class="text-info">Password:</label><br>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="captcha" class="text-info">Captcha:</label><br>
                    <div class="input-group">
                        <button type="button" class="btn btn-info btn-block input-group-text" id="refresh-captcha">Refresh</button>
                        <img src="{{ $captcha }}" alt="captcha" id="captcha" class="img-responsive">
                        <input type="text" name="captcha" id="captcha" class="form-control">
                    </div>
                </div>
                <div class="form-group mt-3">
                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        document.querySelector('#refresh-captcha').addEventListener('click', function () {
            fetch('{{route("refresh_captcha", $bank)}}')
                .then(function (data) {
                    data.text().then(function (text) {
                        document.querySelector('#captcha').src = text;
                    });
                });
        });
    </script>
@endsection
