<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <form class="form-inline" method="post" action="{{route("logout", ['bank' => $bank])}}">
                @csrf
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">logout</button>
            </form>
        </nav>
    </div>
    <div class="container mt-lg-5 align-content-center">
        @yield('content')
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('script')
</body>
</html>
