<?php

namespace App\Contracts\BankCrawlers;

interface BankServiceInterface
{
    public function getChestSession(string $name): mixed;

    public function getAuthSession(string $name): mixed;

    public function checkAuthSessions(): bool;

    public function logout();

    public function login(string $username, string $password, string $captcha);

    public function refreshCaptcha(): string;

    public function getBalances(): array|null;
}
