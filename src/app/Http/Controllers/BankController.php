<?php

namespace App\Http\Controllers;

use App\Contracts\BankCrawlers\BankServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BankController extends Controller
{
    public function loginForm(string $bank)
    {
        $service = app(BankServiceInterface::class, ['bank' => $bank]);
        return view('login', ['captcha' => $service->refreshCaptcha(), 'bank' => $bank]);
    }


    public function login(Request $request, string $bank)
    {
        $service = app(BankServiceInterface::class, ['bank' => $bank]);
        $service->login($request->input('username'), $request->input('password'), $request->input('captcha'));
        return redirect("/balance/$bank");
    }

    public function balance(string $bank)
    {
        $service  = app(BankServiceInterface::class, ['bank' => $bank]);
        $balances = $service->getBalances();
        return view('balance', compact('balances', 'bank'));
    }

    public function logout(string $bank)
    {
        $service = app(BankServiceInterface::class, ['bank' => $bank]);
        $service->logout();
        return redirect("/login/$bank");
    }

    public function refreshCaptcha(string $bank)
    {
        $service = app(BankServiceInterface::class, ['bank' => $bank]);
        return $service->refreshCaptcha();
    }
}
