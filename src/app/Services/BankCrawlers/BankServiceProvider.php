<?php

namespace App\Services\BankCrawlers;

use App\Contracts\BankCrawlers\BankServiceInterface;
use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;

class BankServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(BankServiceInterface::class, function ($app, $params) {
            return match ($params['bank'] ?? null) {
                'pasargad' => new PasargadService(),
                'saman'    => new SamanService(),
                default    => throw new InvalidArgumentException('Bank not found'),
            };
        });
    }
}
