<?php

namespace App\Services\BankCrawlers;

use App\Contracts\BankCrawlers\BankServiceInterface;
use DOMDocument;
use DOMXPath;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use InvalidArgumentException;
use RuntimeException;

class PasargadService implements BankServiceInterface
{
    const VALID_TIME = 5; // 5 minutes

    public function getChestSession(string $name): mixed
    {
        if (
            !Session::get('pasargad_session') ||
            Session::get('pasargad_session.time') <= now()->subMinutes(2)->timestamp
        ) {
            libxml_use_internal_errors(true);
            $res = Http::get('https://ib.bpi.ir');
            $dom = new DOMDocument();
            @$dom->loadHTML($res->body());
            $inputs = [
                '__EVENTARGUMENT'      => $dom->getElementById('__EVENTARGUMENT')?->getAttribute('value'),
                '__LASTFOCUS'          => $dom->getElementById('__LASTFOCUS')?->getAttribute('value'),
                '__VIEWSTATE'          => $dom->getElementById('__VIEWSTATE')->getAttribute('value'),
                '__VIEWSTATEGENERATOR' => $dom->getElementById('__VIEWSTATEGENERATOR')->getAttribute('value'),
                '__EVENTVALIDATION'    => $dom->getElementById('__EVENTVALIDATION')->getAttribute('value'),
            ];
            Session::put('pasargad_session', ['inputs' => $inputs, 'cookies' => $res->cookies(), 'time' => time()]);
        }

        return Session::get("pasargad_session.$name");
    }

    public function getAuthSession(string $name): mixed
    {
        $this->checkAuthSessions();
        return Session::get("pasargad_auth_session.$name");
    }

    public function checkAuthSessions(): bool
    {
        if (
            !Session::get('pasargad_auth_session') ||
            Session::get('pasargad_auth_session.time') <= now()->subMinutes(self::VALID_TIME)->timestamp
        ) {
            return false;
        }
        return true;
    }

    public function logout()
    {
        Session::forget('pasargad_auth_session');
        Session::forget('pasargad_session');
    }

    public function login(string $username, string $password, string $captcha)
    {
        if (!$this->checkAuthSessions()) {
            $cookies = $this->getChestSession('cookies');
            $inputs  = array_merge($this->getChestSession('inputs'), [
                '__EVENTTARGET'      => 'btnLogin',
                'username'           => $username,
                'password'           => $password,
                'captchaTxt'         => $captcha,
                'StatemAgreementent' => 'on',
                'd'                  => now('Asia/Tehran')->format('H:i:s') . ' GMT+0430 (Iran Daylight Time)',
                'hdfOtp'             => null,
                'hdfToken'           => null,

                'DropDownControl$hdfCurrentSelectItem' => null,
                'DropDownControl$hdfSelectedValue'     => 0,
                'DropDownControl$hdfSelectedText'      => 'رمز + ثابت',
                'DropDownControl$hdfSelectedIndex'     => 0,

            ]);
            $res     = Http::send('POST', 'https://ib.bpi.ir/', ['cookies' => $cookies, 'form_params' => $inputs]);

            if (!$res->successful()) {
                throw new RuntimeException('Pasargad login failed');
            }
            libxml_use_internal_errors(true);
            $dom = new DOMDocument();
            $dom->loadHTML($res->body());
            if ($errorNode = $dom->getElementById('lblMessage')) {
                if ($errorNode->nodeValue === 'اطلاعات وارد شده نادرست است.') {
                    throw new InvalidArgumentException('Invalid username or password');
                }
                throw new InvalidArgumentException('Invalid captcha');
            }
            Session::put('pasargad_auth_session', ['cookies' => $res->cookies(), 'time' => time()]);
        }
    }

    public function refreshCaptcha(): string
    {
        $cookies = $this->getChestSession('cookies');
        $inputs  = array_merge($this->getChestSession('inputs'), [
            "__EVENTTARGET"  => "imageBtn",
            "__ASYNCPOST"    => true,
            "ScriptManager1" => "UpdatePanel2|imageBtn",
            "username"       => null,
            "password"       => null,
            "captchaTxt"     => null,
            "d"              => now('Asia/Tehran')->format('H:i:s') . ' GMT+0430 (Iran Daylight Time)',
            "hdfOtp"         => null,
            "hdfToken"       => null,

            'DropDownControl$hdfCurrentSelectItem' => null,
            'DropDownControl$hdfSelectedValue'     => 0,
            'DropDownControl$hdfSelectedText'      => "رمز ثابت",
            'DropDownControl$hdfSelectedIndex'     => 0,
        ]);
        $res     = Http::send('POST', 'https://ib.bpi.ir', [
            'cookies'     => $cookies,
            'form_params' => $inputs,
        ]);
        if (!$res->successful()) {
            throw new RuntimeException('Pasargad captcha refresh failed');
        }
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML($res->body());
        $img   = $dom->getElementById('captchaImg')->getElementsByTagName('img')->item(0)->getAttribute('src');
        $image = Http::send('GET', 'https://ib.bpi.ir/' . $img, ['cookies' => $res->cookies()]);
        return 'data:image/png;base64,' . base64_encode($image->body());
    }

    public function getBalances(): array|null
    {
        $cookies = $this->getAuthSession('cookies');
        if (!$cookies) {
            return null;
        }

        // turn off redirect
        $res = Http::send('GET', 'https://ib.bpi.ir/DepositList.aspx', [
            'cookies'         => $cookies,
            'allow_redirects' => false,
        ]);
        if (!$res->successful()) {
            throw new RuntimeException('Pasargad get balances failed');
        }
        Session::put('pasargad_auth_session', ['cookies' => $res->cookies(), 'time' => time()]);

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML($res->body());
        $xpath = new DOMXPath($dom);
        $rows  = $xpath->query('//table[@id="ctl00_activePage_DepositListTable"]/tbody/tr');

        $balances = [];
        foreach ($rows as $i => $row) {
            if ($i === $rows->length - 1) {
                break;
            }
            $cells                          = (new DOMXPath($dom))->query('td', $row);
            $balances[$i]['deposit_number']  = $cells->item(2)->getAttribute('depositnumber');
            $balances[$i]['iban']            = $cells->item(4)->nodeValue;
            $balances[$i]['amount']          = $cells->item(5)->getElementsByTagName('div')->item(0)->getElementsByTagName('div')->item(0)->nodeValue;
            $balances[$i]['amount_currency'] = $cells->item(5)->getElementsByTagName('div')->item(0)->getElementsByTagName('div')->item(1)->nodeValue;
        }
        return $balances;
    }

}
