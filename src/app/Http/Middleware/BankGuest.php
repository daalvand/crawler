<?php

namespace App\Http\Middleware;

use App\Contracts\BankCrawlers\BankServiceInterface;

class BankGuest
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, $next)
    {
        $service = app(BankServiceInterface::class, ['bank' => $request->route('bank')]);
        if ($service->checkAuthSessions()) {
            return redirect()->route('balance', ['bank' => $request->route('bank')]);
        }
        return $next($request);
    }
}
