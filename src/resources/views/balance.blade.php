@extends('layouts.auth')
@section('content')
    <div class="card">
        <div class="card-body">
            <table class="table-responsive table">
                <tr>
                    <th>Deposit Number</th>
                    <th>IBAN</th>
                    <th>Amount</th>
                    <th>Amount Currency</th>
                </tr>
                @foreach($balances as $balance)
                    <tr>
                        <td>{{ $balance['deposit_number'] }}</td>
                        <td>{{ $balance['iban'] }}</td>
                        <td>{{ $balance['amount'] }}</td>
                        <td>{{ $balance['amount_currency'] }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script>
        setTimeout(function () {
            window.location.reload();
        }, 60000);
    </script>
@endsection
