<?php

use App\Http\Controllers\BankController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('bank_guest')->group(function () {
    Route::get('login/{bank}', [BankController::class, 'loginForm'])->name('login.form');
    Route::post('login/{bank}', [BankController::class, 'login'])->name('login');
    Route::get('refresh_captcha/{bank}', [BankController::class, 'refreshCaptcha'])->name('refresh_captcha');
});

Route::middleware('bank_auth')->group(function () {
    Route::get('balance/{bank}', [BankController::class, 'balance'])->name('balance');
    Route::post('logout/{bank}', [BankController::class, 'logout'])->name('logout');
});
